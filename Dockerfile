FROM tomcat:9.0.34-jdk8-adoptopenjdk-hotspot

USER root

ENV HSQLDB_VERSION 2.3.4

RUN apt-get update \
    && apt-get -y install wget \
    && apt-get -y install unzip


RUN wget http://sourceforge.net/projects/hsqldb/files/hsqldb/hsqldb_2_3/hsqldb-$HSQLDB_VERSION.zip && unzip hsqldb-$HSQLDB_VERSION.zip && rm hsqldb-$HSQLDB_VERSION.zip

#RUN java -cp hsqldb-${HSQLDB_VERSION}/hsqldb/lib/sqltool.jar org.hsqldb.Server -database.0 ..\..\database\amazin -dbname.0 amazin > /etc/db.log
COPY /entrt.sh /usr/tomcat/local/entrt.sh
RUN chmod +x /usr/tomcat/local/entrt.sh


COPY /target/*.war /usr/local/tomcat/webapps/amazin.war

COPY /database /opt/database

EXPOSE 8787
EXPOSE 8080

ENV JPDA_ADDRESS="*:8787"
ENV JPDA_TRANSPORT="dt_socket"

#ENTRYPOINT ["catalina.sh", "run"]
#ENTRYPOINT ["/usr/tomcat/bin/catalina.sh", "jpda", "run"]

ENTRYPOINT ["sh","/usr/tomcat/local/entrt.sh"]
