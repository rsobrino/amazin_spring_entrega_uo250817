package com.miw.persistence.book;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.miw.model.Book;
import com.miw.persistence.Dba;

public class BookDAO implements BookDataService {

	protected Logger logger = Logger.getLogger(getClass());

	@Override
	public List<Book> getBooks() throws Exception {

		List<Book> resultList = null;

		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			resultList = em.createQuery("Select a From Book a", Book.class).getResultList();

			logger.debug("Result list: " + resultList.toString());
			for (Book next : resultList) {
				logger.debug("next book: " + next);
			}

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return resultList;
	}

	@Override
	public Book newBook(Book book) throws Exception {
		// TODO Auto-generated method stub

		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();
			em.persist(book);
			em.getTransaction().commit();

			logger.debug("Alta libro: " + book.toString());

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return book;
	}

	@Override
	public Book getBookByTitle(String title) throws Exception {
		List<Book> resultList = null;
		Book book = null;
		Dba dba = new Dba();
		try {
			EntityManager em = dba.getActiveEm();

			resultList = em.createQuery("SELECT v FROM Book v WHERE v.title = ?", Book.class).setParameter(1, title)
					.getResultList();

			if (resultList.isEmpty()) {
				book = null;
				logger.debug("Get Book: ninigun resultado ");

			} else {
				book = resultList.get(0);
				logger.debug("Get ook: " + resultList.get(0).toString());
			}

		} finally {
			// 100% sure that the transaction and entity manager will be closed
			dba.closeEm();
		}

		// We return the result
		return book;
	}
}