package com.miw.persistence.order;

import java.util.List;

import com.miw.model.Order;

public interface OrderDataService {

	Order newOrder(Order order) throws Exception;

	List<Order> getOrdersByUsername(String username) throws Exception;

}
