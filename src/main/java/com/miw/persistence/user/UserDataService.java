package com.miw.persistence.user;

import com.miw.model.User;

public interface UserDataService {

	User getUser(String username) throws Exception;

	User newUser(User user) throws Exception;

}
