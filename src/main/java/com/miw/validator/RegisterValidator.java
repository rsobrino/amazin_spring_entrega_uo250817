package com.miw.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.miw.model.RegisterInfo;

public class RegisterValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return RegisterInfo.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		RegisterInfo registerInfo = (RegisterInfo) target;

		if (!registerInfo.getPassword().equals(registerInfo.getPasswordRepeat())) {
			errors.rejectValue("password", "", "Las contraseņas no coinciden");
		}

	}

}
