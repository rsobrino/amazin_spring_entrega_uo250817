
package com.miw.business.bookmanager;

import java.util.List;

import org.apache.log4j.Logger;

import com.miw.model.Book;

public class BookManagerEJBService implements BookManagerService {
	Logger logger = Logger.getLogger(this.getClass());

	private Integer repetitions = 1;
	private BookManager bookManager = null;

	public BookManager getBookManager() {
		return bookManager;
	}

	public void setBookManager(BookManager bookManager) {
		this.bookManager = bookManager;
	}

	public Integer getRepetitions() {
		return repetitions;
	}

	public void setRepetitions(Integer repetitions) {
		this.repetitions = repetitions;
	}

	@Override
	public List<Book> getBooks() throws Exception {
		printMessage("*** BookManagerEJBService.getBooks();");
		return bookManager.getBooks();
	}

	@Override
	public Book getSpecialOffer() throws Exception {
		printMessage("*** BookManagerEJBService.getSpecialOffer();");
		return bookManager.getSpecialOffer();
	}

	private void printMessage(String message) {
		for (int i = 0; i < repetitions; i++) {
			System.out.println(message);
		}
	}

	@Override
	public Book newBook(Book book, int family) throws Exception {
		return bookManager.newBook(book, family);
	}

	@Override
	public Book getBookByTitle(String title) throws Exception {
		printMessage("*** BookManagerEJBService.getBookByTitle();");
		return bookManager.getBookByTitle(title);
	}
}
