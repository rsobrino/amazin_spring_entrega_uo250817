package com.miw.business.usermanager;

import com.miw.model.User;
import com.miw.persistence.user.UserDataService;

public class UserManager implements UserManagerService {

	private UserDataService userDataService = null;

	@Override
	public User getUser(String username) throws Exception {
		return userDataService.getUser(username);
	}

	@Override
	public User newUser(User user) throws Exception {
		return userDataService.newUser(user);
	}

	public UserDataService getUserDataService() {
		return userDataService;
	}

	public void setUserDataService(UserDataService userDataService) {
		this.userDataService = userDataService;
	}

}
