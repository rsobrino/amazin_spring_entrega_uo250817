package com.miw.business.ordermanager;

import java.util.List;
import java.util.Map;

import com.miw.model.Order;
import com.miw.model.User;

public interface OrderManagerService {

	Order newOrder(Order order) throws Exception;

	List<Order> getOrdersByUsername(String username) throws Exception;

	Order createOrderByShoppingCartAndUserInfo(Map<String, Integer> shoppingCart, User loginInfo) throws Exception;

}
