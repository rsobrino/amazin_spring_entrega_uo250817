package com.miw.business.ordermanager;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.miw.business.bookmanager.BookManagerService;
import com.miw.model.Book;
import com.miw.model.Order;
import com.miw.model.User;
import com.miw.persistence.order.OrderDataService;

public class OrderManager implements OrderManagerService {

	private OrderDataService orderDataService = null;

	private BookManagerService bookManagerService = null;

	@Override
	public Order newOrder(Order order) throws Exception {
		return orderDataService.newOrder(order);
	}

	@Override
	public List<Order> getOrdersByUsername(String username) throws Exception {
		return orderDataService.getOrdersByUsername(username);
	}

	@Override
	public Order createOrderByShoppingCartAndUserInfo(Map<String, Integer> shoppingCart, User user) throws Exception {

		int numeroLibros = 0;
		double total = 0.0;
		Object[] keys = shoppingCart.keySet().toArray();
		for (int i = 0; i < keys.length; i++) {
			Book book = bookManagerService.getBookByTitle((String) keys[i]);
			numeroLibros += shoppingCart.get(keys[i]);
			total += book.getPrice() * shoppingCart.get(keys[i]);
		}

		Order order = new Order();
		order.setCode(UUID.randomUUID().toString().substring(0, 7));
		order.setUsername(user.getUsername());
		order.setNumeroLibros(numeroLibros);
		order.setTotal(redondearDecimales(total, 2));
		order.setFecha(Date.from(Instant.now()));

		return order;
	}

	public static double redondearDecimales(double valorInicial, int numeroDecimales) {
		double parteEntera, resultado;
		resultado = valorInicial;
		parteEntera = Math.floor(resultado);
		resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
		resultado = Math.round(resultado);
		resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
		return resultado;
	}

	public OrderDataService getOrderDataService() {
		return orderDataService;
	}

	public void setOrderDataService(OrderDataService orderDataService) {
		this.orderDataService = orderDataService;
	}

	public BookManagerService getBookManagerService() {
		return bookManagerService;
	}

	public void setBookManagerService(BookManagerService bookManagerService) {
		this.bookManagerService = bookManagerService;
	}

}
