package com.miw.presentation;

import java.util.Map;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.miw.business.ordermanager.OrderManagerService;
import com.miw.business.usermanager.UserManagerService;
import com.miw.model.Order;
import com.miw.model.User;

@SessionAttributes({ "shoppingCart" })
@Controller
public class AddOrderController {

	Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserManagerService userManagerService;

	@Autowired
	private OrderManagerService orderManagerService;

	private Order order;

	public User getUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDetails userDetail = (UserDetails) auth.getPrincipal();
		try {
			return this.userManagerService.getUser(userDetail.getUsername());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(path = "private/addOrder", method = RequestMethod.GET)
	public String showAddOrder(@ModelAttribute("shoppingCart") Map<String, Integer> shoppingCart, Model model)
			throws Exception {
		logger.debug("Executing showAddOrder");

		this.order = this.orderManagerService.createOrderByShoppingCartAndUserInfo(shoppingCart, getUser());
		model.addAttribute("order", this.order);
		return "private/addOrder";
	}

	@RequestMapping(path = "private/addOrder", method = RequestMethod.POST)
	public String addOrder(@ModelAttribute("shoppingCart") Map<String, Integer> shoppingCart,
			@Valid @ModelAttribute("order") Order order, BindingResult result) throws Exception {

		logger.debug("Executing addOrder");

		if (result.hasErrors()) {
			return "private/addOrder";
		}

		this.orderManagerService.newOrder(order);

		shoppingCart.clear();

		order = null;

		return "private/index";
	}

	@ModelAttribute("order")
	public Order getOrder() {
		if (order == null) {
			order = new Order();
		}
		return order;
	}

}
