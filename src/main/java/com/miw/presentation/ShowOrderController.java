package com.miw.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.miw.business.ordermanager.OrderManagerService;

@Controller
public class ShowOrderController {

	@Autowired
	private OrderManagerService orderManagerService;

	public void setOrderManagerService(OrderManagerService orderManagerService) {
		this.orderManagerService = orderManagerService;
	}

	@RequestMapping("private/showOrder")
	public String showOrder(Model model) throws Exception {
		System.out.println("Executing showOrder.");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDetails userDetail = (UserDetails) auth.getPrincipal();

		model.addAttribute("orderlist", orderManagerService.getOrdersByUsername(userDetail.getUsername()));
		return "private/showOrder";

	}
}
