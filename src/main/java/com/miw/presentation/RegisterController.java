package com.miw.presentation;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.business.usermanager.UserManagerService;
import com.miw.model.RegisterInfo;
import com.miw.model.User;
import com.miw.validator.RegisterValidator;

@Controller
@RequestMapping("register")
public class RegisterController {

	Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserManagerService userManagerService;

	@RequestMapping(method = RequestMethod.GET)
	public String register() {
		logger.debug("Preparing the model for register");
		return "register";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String addRegister(@Valid @ModelAttribute RegisterInfo registerInfo, BindingResult result) {

		RegisterValidator registerValidator = new RegisterValidator();
		registerValidator.validate(registerInfo, result);

		try {
			if (this.userManagerService.getUser(registerInfo.getUsername()) != null) {
				logger.debug("El usuario ya existe");
				result.rejectValue("username", "", "El usuario ya existe");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (result.hasErrors()) {
			return "register";
		}

		try {
			User user = new User();
			user.setUsername(registerInfo.getUsername());
			user.setPassword(registerInfo.getPassword());
			user.setMail(registerInfo.getEmail());
			user.setRole("ROLE_ADMIN");
			this.userManagerService.newUser(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:login";
	}

	public UserManagerService getUserManagerService() {
		return userManagerService;
	}

	public void setUserManagerService(UserManagerService userManagerService) {
		this.userManagerService = userManagerService;
	}

	@ModelAttribute
	private RegisterInfo getRegisterInfo() {
		return new RegisterInfo();
	}

}
