package com.miw.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Length;

@Entity
public class Order {

	@Id
	@GeneratedValue
	private int id;
	@Length(min = 1, max = 8, message = "Debe tener entre 1 y 8 caracteres.")
	private String payment;
	private String code;
	@Length(min = 1, max = 25, message = "Debe tener entre 1 y 25 caracteres.")
	private String address;
	private String username;

	private int numeroLibros;
	private double total;
	private Date fecha;

	public Order() {
		super();
	}

	public Order(int id, String payment, String code, String address, String username) {
		super();
		this.id = id;
		this.payment = payment;
		this.code = code;
		this.address = address;
		this.username = username;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getNumeroLibros() {
		return numeroLibros;
	}

	public void setNumeroLibros(int numeroLibros) {
		this.numeroLibros = numeroLibros;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", payment=" + payment + ", code=" + code + ", address=" + address + ", username="
				+ username + ", numeroLibros=" + numeroLibros + ", total=" + total + "]";
	}

}
