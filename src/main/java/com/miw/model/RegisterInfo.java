package com.miw.model;

import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.Length;

public class RegisterInfo {

	@Length(min = 1, max = 8, message = "Debe tener entre 1 y 8 caracteres.")
	private String username;
	@Email
	private String email;
	@Length(min = 1, max = 8, message = "Debe tener entre 1 y 8 caracteres.")
	private String password;
	@Length(min = 1, max = 8, message = "Debe tener entre 1 y 8 caracteres.")
	private String passwordRepeat;

	public RegisterInfo() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	@Override
	public String toString() {
		return "RegisterInfo [username=" + username + ", email=" + email + ", password=" + password
				+ ", passwordRepeat=" + passwordRepeat + "]";
	}

}
