<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html >
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<title><spring:message code="index.h1"/></title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body>
	<header>
		<h1 class="header"><spring:message code="index.h1"/></h1>
		<h2 class="centered">
			<spring:message code="index.h2.part1"/><em/> <spring:message code="index.h2.part2"/><em/><spring:message code="index.h2.part3"/>
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="menu"><spring:message code="index.nav.list1"/></a></li>
			<li><a href="http://miw.uniovi.es"><spring:message code="index.nav.list2"/></a></li>
			<li><a href="mailto:dd@email.com"><spring:message code="index.nav.list3"/></a></li>
			<li><a href="newBook"><spring:message code="index.nav.list4"/></a></li>
			<li><a href="shoppingCart"><spring:message code="index.nav.list5"/></a></li>
			<li><a href="showShoppingCart"><spring:message code="index.nav.list6"/></a></li>
		</ul>
	</nav>
	<section>
		<article>
			<label class="mytitle"><spring:message code="show.newBook"/></label><br />
			
			<form:form modelAttribute="order">
				<form:errors path="" />
				<br />
				<spring:message code="order.code"/>: <form:input path="code" disabled="true"/>
				<br />
				<spring:message code="order.payment"/>: <form:input path="payment" />
				<form:errors path="payment" />
				<br />
				<spring:message code="order.address"/>: <form:input path="address" />
				<form:errors path="address" />
				<br />
				<spring:message code="order.numerolibros"/>: <form:input path="numeroLibros" disabled="true"/>
				<br />
				<spring:message code="order.total"/>: <form:input path="total" disabled="true" />
				<br />
				<input type="submit" />
			</form:form>
			<p style="color: red;">
				<c:out value="${message}" />
			</p>
		</article>
	</section>
	<footer>
		<strong><spring:message code="index.footer.part1"/></strong><br />
		<em><spring:message code="index.footer.part2"/></em>
	</footer>
</body>