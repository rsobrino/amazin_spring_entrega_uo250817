<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html >
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<title><spring:message code="index.h1"/></title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body>
	<header>
		<h1 class="header"><spring:message code="index.h1"/></h1>
		<h2 class="centered">
			<spring:message code="index.h2.part1"/><em/> <spring:message code="index.h2.part2"/><em/><spring:message code="index.h2.part3"/>
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="menu"><spring:message code="index.nav.list1"/></a></li>
			<li><a href="http://miw.uniovi.es"><spring:message code="index.nav.list2"/></a></li>
			<li><a href="mailto:dd@email.com"><spring:message code="index.nav.list3"/></a></li>
			<li><a href="newBook"><spring:message code="index.nav.list4"/></a></li>
			<li><a href="shoppingCart"><spring:message code="index.nav.list5"/></a></li>
			<li><a href="showShoppingCart"><spring:message code="index.nav.list6"/></a></li>
		</ul>
	</nav>
	<section>
		<article>
			<label class="mytitle"><spring:message code="show.newBook"/></label><br />
			
			<form:form modelAttribute="book">
				<form:errors path="" />
				<br />
				<spring:message code="show.title"/>: <form:input path="title" />
				<form:errors path="title" />
				<br />
				<spring:message code="show.description"/>: <form:input path="description" />
				<form:errors path="description" />
				<br />
				<spring:message code="show.author"/>: <form:input path="author" />
				<form:errors path="author" />
				<br />
				<spring:message code="show.baseprice"/>: <form:input path="basePrice" />
				<form:errors path="basePrice" />
				<br />
				<input type="submit" />
			</form:form>
			<p style="color: red;">
				<c:out value="${message}" />
			</p>
		</article>
	</section>
	<footer>
		<strong><spring:message code="index.footer.part1"/></strong><br />
		<em><spring:message code="index.footer.part2"/></em>
	</footer>
</body>