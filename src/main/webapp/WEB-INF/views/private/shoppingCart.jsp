<!DOCTYPE html >
<%@ page contentType="text/html; charset=iso-8859-1"
	pageEncoding="iso-8859-1" language="java"
	import="java.util.*, com.miw.model.Book,com.miw.presentation.book.*"
	errorPage=""%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page isELIgnored="false"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<head>
<title><spring:message code="index.h1"/></title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body>
	<header>
		<h1 class="header"><spring:message code="index.h1"/></h1>
		<h2 class="centered">
			<spring:message code="index.h2.part1"/> <em/> <spring:message code="index.h2.part2"/><em/><spring:message code="index.h2.part3"/>
		</h2>
	</header>
	<nav>
		<ul>
			<li><a href="menu"><spring:message code="index.nav.list1"/></a></li>
			<li><a href="http://miw.uniovi.es"><spring:message code="index.nav.list2"/></a></li>
			<li><a href="mailto:dd@email.com"><spring:message code="index.nav.list3"/></a></li>
			<li><a href="newBook"><spring:message code="index.nav.list4"/></a></li>
			<li><a href="shoppingCart"><spring:message code="index.nav.list5"/></a></li>
			<li><a href="showShoppingCart"><spring:message code="index.nav.list6"/></a></li>
		</ul>
	</nav>
	<section>
		<article>
					
			<form name="addToShoppingCart" action="addToShoppingCart" method="post">
				<table>
					<caption><spring:message code="shopping.title"/>:</caption>
					<thead>
						<tr>
							<th><spring:message code="shopping.select"/></th>
							<th><spring:message code="show.title"/></th>
							<th><spring:message code="show.author"/></th>
							<th><spring:message code="show.description"/></th>
							<th><spring:message code="show.price"/></th>
						</tr>
					</thead>
	
					<tbody>
						<c:forEach var='book' items="${booklist}">
							<tr>
								<td><input type="checkbox" name="${book.id}"/></td>
								<td><c:out value="${book.title}" /></td>
								<td><c:out value="${book.author}" /></td>
								<td><c:out value="${book.description}" /></td>
								<td><c:out value="${book.price}" /> &euro;</td>
							</tr>						
						</c:forEach>
					</tbody>
				</table>
				
				<input type="submit" value="<spring:message code="form.add"/>"/>
			</form>
		</article>
	</section>
	<footer>
		<strong><spring:message code="index.footer.part1"/></strong><br />
		<em><spring:message code="index.footer.part2"/></em>
	</footer>
</body>